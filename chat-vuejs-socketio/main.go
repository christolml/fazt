package main

import (
	"github.com/googollee/go-socket.io"
	"github.com/labstack/gommon/log"
	"net/http"
)

func main() {
	server, err := socketio.NewServer(nil)
	if err != nil {
		log.Fatal(err)
	}

	// sockets
	// cuando se acceda al servidor el usuario activa el socket de connection y me guarda
	// su sesion en la variable so
	server.On("connection", func(so socketio.Socket) {
		log.Print("on connection")

		// events
		// cuando un usuario entra a la app se le une a una sala con el nombre de chat_room
		so.Join("chat_room")
		// el servidor esta escuchando el event chat message que viene del cliente y cuando recibe
		// el mensaje lo retransmite a todos los usuarios de la sala de chat con so.Emit
		so.On("chat message", func(msg string) {
			log.Print("emit: ", so.Emit("chat message", msg))
			so.BroadcastTo("chat_room", "chat message", msg)
		})
	})

	http.Handle("/socket.io/", server)
	http.Handle("/", http.FileServer(http.Dir("./public")))
	log.Print("Server on port 3000")
	log.Fatal(http.ListenAndServe(":3000", nil))
}
